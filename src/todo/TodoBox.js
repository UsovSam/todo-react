/**
 * Created by Sam.
 */
import React, {Component} from 'react';
import Item from './Item';
import './TodoBox.css'

class TodoBox extends Component {

    constructor(props) {
        super(props);
        this.state = {text: '', list: [], sum: 0, done: 0, todo: 0};
    }

    handleChange = (e) => {
        this.setState({text: e.target.value});
    };

    onClick = (e) => {
        let text = this.state.text;
        if (!!text) {
            this.setState((prevState, props) => (
                {
                    list: [ ...prevState.list, text ],
                    text: '',
                    sum: prevState.sum + 1,
                    todo: prevState.todo + 1
                }));

        }
        this.focus();
    };

    focus = () => {
        this.textInput.focus();
    };

    done = () => {
        this.setState((prevState, props) => ({
            done: prevState.done + 1,
            todo: prevState.todo - 1

        }));
    };

    componentDidMount() {
        this.textInput.focus();
    }

    render() {
        return (
            <div className="TodoBox">
                <div>
                    <input type="text"
                           placeholder="input"
                           value={this.state.text}
                           onChange={this.handleChange}
                           ref={(input) => {
                               this.textInput = input;
                           }}
                    />
                    <button onClick={this.onClick}>ok</button>
                </div>
                <div>
                    <span>Всего: {this.state.sum} </span>
                    <span>Осталось: {this.state.todo} </span>
                    <span>Сделано: {this.state.done} </span>
                </div>
                <ul className="List">
                    {
                        this.state.list.map((el, i) =>
                            <Item key={i} id={'todoItem_' + i} text={el} onDone={this.done}/>
                        )
                    }
                </ul>
            </div>
        );
    }

}

export default TodoBox;

