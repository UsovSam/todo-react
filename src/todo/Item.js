/**
 * Created by Sam.
 */
import React, {Component} from 'react';

class Item extends Component {

    constructor(props) {
        super(props);
        this.state = {checked: false}
    }

    changeStatus = (e) => {
        if (!this.state.checked) {
            this.setState({checked: e.target.checked});
            this.props.onDone();
        }

    };

    render() {
        return (
            <li>
                <input id={this.props.id}
                    type="checkbox"
                    disabled={this.state.checked}
                    onChange={this.changeStatus}
                />
                <label htmlFor={this.props.id}>
                    {this.props.text}
                </label>
            </li>
        );
    }

}

export default Item;