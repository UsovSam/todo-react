/**
 * Created by Sam.
 */
import React, {Component} from 'react';


class List extends Component {

    constructor(props) {
        super(props);
        let sum = this.props.list.length;
        this.state = {sum: sum, done: 0, todo: sum}
    }

    done = () => {
        this.setState((prevState, props) => ({
            done: prevState.done + 1,
            todo: prevState.todo - 1

        }));
    };


    render() {
        return (
            <div>
                <div>
                    <span>Всего: {this.state.sum} </span>
                    <span>Осталось: {this.state.todo} </span>
                    <span>Сделано: {this.state.done} </span>
                </div>

            </div>
        );
    }
}

export default List;